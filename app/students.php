<?php

namespace App;

use App\subdistrict;
use App\city;
use Illuminate\Database\Eloquent\Model;

class students extends Model
{
    public function subdistrict()
    {
        return $this->belongsTo(Subdistrict::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
