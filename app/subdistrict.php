<?php

namespace App;

use App\city;
use App\students;
use Illuminate\Database\Eloquent\Model;

class subdistrict extends Model
{
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function student()
    {
        return $this->hasOne(Students::class);
    }
}
