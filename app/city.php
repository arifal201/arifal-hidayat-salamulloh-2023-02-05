<?php

namespace App;
use App\subdistrict;

use Illuminate\Database\Eloquent\Model;

class city extends Model
{
    public function subdistrict()
    {
        return $this->belongsTo('App\subdistrict');
    }
}
