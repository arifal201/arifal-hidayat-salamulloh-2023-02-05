<?php

namespace App\Http\Controllers;

use App\subdistrict;
use Illuminate\Http\Request;

class SubdistrictController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subdistricts = subdistrict::all();
        return view('subdistricts.index')->compact('subdistrict');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('subdistrict.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\subdistrict  $subdistrict
     * @return \Illuminate\Http\Response
     */
    public function show(subdistrict $subdistrict)
    {
        return view('subdistrict.show')->compact('subdistrict');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\subdistrict  $subdistrict
     * @return \Illuminate\Http\Response
     */
    public function edit(subdistrict $subdistrict)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\subdistrict  $subdistrict
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, subdistrict $subdistrict)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\subdistrict  $subdistrict
     * @return \Illuminate\Http\Response
     */
    public function destroy(subdistrict $subdistrict)
    {
        $subdistrict->delete();
        return redirect()->route('subdistrict.index')->with('success','subdistrict Deleted');
    }
}
