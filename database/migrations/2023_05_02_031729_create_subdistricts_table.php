<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubdistrictsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subdistricts', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('city_id');
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
            $table->timestamps();
        });

        DB::table('subdistricts')->insert([
            ['name' => 'Bandung Timur', 'city_id' => 1],
            ['name' => 'Cimahi Utara', 'city_id' => 2],
            ['name' => 'Padalarang', 'city_id' => 3],
            ['name' => 'Cimahi Tengah', 'city_id' => 2],
            ['name' => 'Antapani', 'city_id' => 1],
            ['name' => 'Lembang', 'city_id' => 3],
            ['name' => 'Cimahi Selatan', 'city_id' => 2],
            ['name' => 'Batujajar', 'city_id' => 3]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subdistricts');
    }
}
