<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Route::get('/cities', 'CityController@index');
// Route::get('/cities/{cities}', 'CityController@show');
// Route::post('/cities', 'CityController@create');
// Route::get('/students', 'StudentsController@index');
// Route::get('/students/{students}', 'StudentsController@show');
Route::resources([
    'cities' => 'CityController',
    'students' => 'StudentsController',
    'subdistrict' => 'SubdistrictController'
]);