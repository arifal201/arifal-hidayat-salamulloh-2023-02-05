@extends('layout')

@section('content')
<form action="{{route('students.store')}}" method="post">
  <div class="form-group">
    @csrf
    <label for="name">Student Name:</label>
    <input type="text" class="form-control" name="name"/>
  </div>
  <div class="form-group">
    <label for="symptoms">City :</label>
    @foreach ($cities as $key => $value)
      <option value="{{ $key }}">
        {{$value->name}}
      </option>
      <div class="form-group">
        <label for="symptoms">District :</label>
        @foreach ($cities->subdistrict()->get() as $district)
          <option value="{{$district->id}}">
            {{$district->name}}
          </option>
        @endforeach

      </div>
    @endforeach
  </div>
  <div class="form-group">
    <label for="cases">Address :</label>
    <textarea rows="5" columns="5" class="form-control" name="address"></textarea>
  </div>
  <button type="submit" class="btn btn-primary">Add Data</button>
</form>
@stop